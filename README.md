# dbms_proj
# These Instructions are for a Unix/Linux system.

This application was built using Python3 and HTML.
The package manager pip was used and is required. 
    - pip >= 9.0.1 is supported

Create mysql database using:
    create.sql and insert.sql

With a text editor:
    - open dbms_proj/proj/db.py
    - edit lines 10-12 to match your local settings
    - save

In a terminal, navigate to the project root.
    - Execute :
    - `python3 -m venv "your environment name" `
    - `source "your environment name"/bin/activate`
    - `pip install -r requirements.txt`
    - `export FLASK_APP=proj`
    - `export FLASK_ENV=development`
    - `flask run`

Navigate to: 
    - http://127.0.0.1:5000/
    - check out the running project




