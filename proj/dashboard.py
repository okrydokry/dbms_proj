from proj.db import Database
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)

from werkzeug.exceptions import abort


bp = Blueprint('dashboard', __name__)

"""
select CI.Industry_Name, CI.Company_Symbol
from GradSquad_Tables.COMPANY_INSTITUTION as CI, GradSquad_Tables.Industry as I
where CI.Industry_Name=I.Industry_Name
group by CI.Company_Symbol

"""


@bp.route('/', methods=['GET', 'POST'])
def index():

    res = db_get_industries()
    exchanges = db_get_exchanges()
    company_symbols = db_get_company_symbols()
    owner_ids = db_get_owner_ids()
    # print(company_symbols)
    asset_types= db_get_asset_types()
    interest_backing_countries = db_get_interest_backing_countries()
    ind_comp = None
    securities = None
    daily_prices = None
    asset_owners = None
    portfolio = None
    event_report = None
    event_loc_report = None
    interest_report = None
    fin_report = None
    asset_event_report = None
    price_event_report = None
    if request.method == 'POST':
        try:
            ind_comp = func_req_1()
        except Exception as e:
            print(e)
        
        try:
            securities = func_req_2()
        except Exception as e:
            print(e)
        
        try:
            daily_prices = func_req_3()
        except Exception as e:
            print(e)
        
        try:
            asset_owners = func_req_4()
        except Exception as e:
            print(e)

        try:
            portfolio = func_req_5()
        except Exception as e:
            print(e)

        try:
            event_report = func_req_6()
        except Exception as e:
            print(e)
        
        try:
            event_loc_report = func_req_7()
        except Exception as e:
            print(e)

        try:
            interest_report = func_req_8()
        except Exception as e:
            print(e)

        try:
            fin_report = func_req_9()
        except Exception as e:
            print(e)

        try:
            asset_event_report = func_req_10()
        except Exception as e:
            print(e)
        
        try:
            price_event_report = func_req_11()
        except Exception as e:
            print(e)

    return render_template('index.html',
                           result=res,
                           exchanges=exchanges,
                           company_symbols=company_symbols,
                           owner_ids=owner_ids,
                           ind_comp=ind_comp, 
                           asset_types=asset_types,
                           assets=securities,
                           daily_prices=daily_prices,
                           asset_owners=asset_owners,
                           portfolio=portfolio,
                           event_report=event_report,
                           interest_backing_countries=interest_backing_countries,
                           event_loc_report=event_loc_report,
                           interest_report=interest_report,
                           fin_report=fin_report,
                           asset_event_report=asset_event_report,
                           price_event_report=price_event_report,
                           content_type='application/json')


def db_get_companies():
    db = Database()
    companies = db.list_companies()

    return companies

def db_get_company_symbols():
    db = Database()
    result = db.list_company_symbols()
    return result

def db_get_interest_backing_countries():
    db = Database()
    result = db.list_interest_backing_countries()
    return result

def db_get_asset_types():
    db = Database()
    asset_types = db.list_asset_types()
    return asset_types

def db_get_exchanges():
    db = Database()
    exchanges = db.list_exchanges()

    return exchanges

def db_get_industries():
    db = Database()
    industries = db.list_industries()

    return industries

def db_get_owner_ids():
    db = Database()
    result = db.list_owner_ids()
    return result

def db_get_companies_in_industry(industry_name, limit):
    db = Database()
    result = db.list_companies_in_industry(industry_name, limit)
    return result

def db_get_securities_on_exchange(exchange_name, limit):
    db = Database()
    result = db.list_securities_on_exchange(exchange_name, limit)
    return result

def db_get_daily_prices(price_type, date, limit):
    db = Database()
    result = db.list_daily_asset_prices(price_type, date, limit)
    return result

def db_get_asset_owners(asset_type, company_symbol, limit):
    db = Database()
    result = db.list_asset_owners(asset_type, company_symbol, limit)
    return result

def db_get_portfolio(owner_id):
    db = Database()
    result = db.list_owner_portfolio(owner_id)
    return result

def db_get_event_impact(date):
    db = Database()
    result = db.list_event_impact(date)
    return result

def db_get_event_location_impact(date, limit):
    db = Database()
    result = db.impact_by_location(date, limit)
    return result

def db_get_daily_price_with_ir(date, price_type, country_symbol, limit):
    db = Database()
    result = db.list_price_with_ir(date, price_type, country_symbol, limit)
    return result

def db_get_price_with_fin_data(comparator, date, company_symbol):
    db = Database()
    result = db.list_price_with_fin_data(comparator, date, company_symbol)
    return result

def db_get_fin_data_with_event(date, limit):
    db = Database()
    result = db.list_fin_data_event_impact(date, limit)
    return result

def db_get_price_event_report(date):
    db = Database()
    result = db.list_price_with_event_impact(date)
    return result

def func_req_1():
    try:
        industry_name = request.form['industry_name']
        if request.form['ind_comp_limit']:
            ind_comp_limit = int(request.form['ind_comp_limit'])
        else:
            ind_comp_limit = 100
        ind_comp = db_get_companies_in_industry(industry_name,
                                                ind_comp_limit)
    except Exception as e:
        print("query 1")
        print(e)

    return ind_comp

def func_req_2():
    try:
        exchange_name = request.form['exchange_name']
        if request.form['asset_limit']:
            asset_limit = int(request.form['asset_limit'])
        else:
            asset_limit = 100
        securities = db_get_securities_on_exchange(exchange_name,
                                                    asset_limit)
    except Exception as e:
        print("query 2")
        print(e)
    return securities

def func_req_3():
    try:
        price_type =  request.form['price_type']
        price_month = request.form['price_month']
        price_day = request.form['price_day']
        price_date = "2019-{0}-{1}".format(price_month,
                                            price_day)
        # print(price_date)
        if request.form['price_limit']:
            price_limit = int(request.form['price_limit'])
        else:
            price_limit = 1000
        daily_prices = db_get_daily_prices(price_type,
                                        price_date,
                                        price_limit)
    except Exception as e:
        print("query 3")
        print(e)
    return daily_prices

def func_req_4():
    try:
        company_symbol = request.form['owns_company_symbol']
        asset_type = request.form['owns_asset_type']
        if request.form['owns_limit']:
            limit = request.form['owns_limit']
        else:
            limit = 1000
        asset_owners = db_get_asset_owners(asset_type,
                                            company_symbol,
                                            limit)
        print(asset_owners)
    except Exception as e:
        print("Query 4")
        print(e)
    return asset_owners

def func_req_5():
    try:
        owner_id = request.form['portf_owner_id']
        portfolio = db_get_portfolio(owner_id)
    except Exception as e:
        print(e)
    return portfolio

def func_req_6():
    try:
        month = request.form['event_month']
        day = request.form['event_day']
        date = "2019-{0}-{1}".format(month, day)
        event_report = db_get_event_impact(date)
    except Exception as e:
        print(e)
    return event_report

def func_req_7():
    try:
        month = request.form['event_loc_month']
        day = request.form['event_loc_day']
        date = "2019-{0}-{1}".format(month, day)
        if request.form['event_loc_limit']:
            limit = request.form['event_loc_limit']
        else:
            limit = 1000
        event_loc_report = db_get_event_location_impact(date, limit)
        print("here")
        
    except Exception as e:
        print(e)
    return event_loc_report

def func_req_8():
    try:
        month = request.form['int_month']
        day = request.form['int_day']
        country_symbol = request.form['int_country']
        price_type = request.form['int_price_type']
        date = "2019-{0}-{1}".format(month, day)
        if request.form['int_limit']:
            limit = request.form['int_limit']
        else:
            limit = 1000
        interest_report = db_get_daily_price_with_ir(date,
                                                     price_type,
                                                     country_symbol,
                                                     limit)
    except Exception as e:
        print(e)
    return interest_report

def func_req_9():
    try:
        month = request.form['fin_data_month']
        day = request.form['fin_data_day']
        company = request.form['fin_data_company_symbol']
        comparator = request.form['fin_gt_lt']
        date = "2019-{0}-{1}".format(month, day)
        fin_report = db_get_price_with_fin_data(comparator,
                                                date,
                                                company)
    except Exception as e:
        print(e)
    return fin_report

def func_req_10():
    try:
        month = request.form['asset_event_month']
        day = request.form['asset_event_day']
        date = "2019-{0}-{1}".format(month, day)

        if request.form['asset_event_limit']:
            limit = request.form['asset_event_limit']
        else:
            limit = 1000
        asset_event_report = db_get_fin_data_with_event(date,
                                                        limit)
    except Exception as e:
        print(e)
    return asset_event_report

def func_req_11():
    try:
        month = request.form['price_event_month']
        day = request.form['price_event_day']
        date = "2019-{0}-{1}".format(month, day)
        price_event_report = db_get_price_event_report(date)
    except Exception as e:
        print(e)
    return price_event_report