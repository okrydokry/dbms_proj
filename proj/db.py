import pymysql
from datetime import datetime
# import click
# from flask import current_app, g
# from flask.cli import with_appcontext


class Database:
    def __init__(self):
        host = "127.0.0.1"
        user = "root"
        password = "password"
        db = "GradSquad_Tables"
        self.con = pymysql.connect(host=host, user=user, password=password,
                                   db=db,
                                   cursorclass=pymysql.cursors.DictCursor)
        self.cur = self.con.cursor()

    def list_companies(self):
        self.cur.execute("""

        SELECT company_name
        FROM company_institution 
        order by company_name;

        """)
        result = self.cur.fetchall()

        return result

    def list_interest_backing_countries(self):
        query = """
        SELECT rf.country_symbol
        from Risk_Free_Rate as rf
        group by rf.country_symbol
        """
        self.cur.execute(query)
        result = self.cur.fetchall()
        return result

    def list_owner_ids(self):
        query = """
        select owner_id
        from owner_m
        order by owner_id
        """
        self.cur.execute(query)
        result = self.cur.fetchall()
        return result

    def list_company_symbols(self):
        self.cur.execute("""

        SELECT company_symbol
        FROM company_institution 
        order by company_symbol;

        """)
        result = self.cur.fetchall()

        return result

    def list_asset_types(self):
        query = """
        SELECT asset_type
        FROM ASSET
        group by asset_type;
        """
        self.cur.execute(query)
        result = self.cur.fetchall()
        return result

    def list_industries(self):
        self.cur.execute("""

        SELECT industry_name
        FROM industry ;

        """)
        result = self.cur.fetchall()

        return result

    def list_exchanges(self):
        self.cur.execute("""

        SELECT exchange_name
        FROM exchange_m ;

        """)
        result = self.cur.fetchall()

        return result

    def list_companies_in_industry(self, industry_name, limit):
        query = """
                SELECT c.company_name, c.company_symbol
                From company_institution as c, industry as i
                where c.industry_name = '{0}'
                group by c.Company_Symbol
                LIMIT {1};
                """.format(industry_name, limit)
        # print(query)
        self.cur.execute(query)

        result = self.cur.fetchall()
        return result

    def list_securities_on_exchange(self, exchange_name, limit):
        query ="""
            select C.company_name, A.asset_type
            from (
	            select offers.company_symbol
                from Offers_Securities as offers
                where offers.exchange_name='{0}'
                ) as intermediate, Asset as A, Company_Institution as C
            where A.company_symbol=intermediate.company_symbol
                AND C.company_symbol=intermediate.company_symbol
            group by C.company_name, A.asset_type
            order by C.company_name
            LIMIT {1};
               """.format(exchange_name, limit)
        self.cur.execute(query)
        result = self.cur.fetchall()
        return result
    
    def list_daily_asset_prices(self, price_type, date, limit):
        query = """
        select DP.company_symbol, DP.asset_type, DP.{0} AS price, DP.time_stamp
        from DAILY_PRICE as DP
        where Date(DP.time_stamp)=Date('{1}')
        group by DP.company_symbol, DP.asset_type, price, DP.time_stamp
        LIMIT {2};
        """.format(price_type, date, limit)
        # print(query)
        self.cur.execute(query)
        result = self.cur.fetchall()
        return result

    def list_asset_owners(self, asset_type, company_symbol, limit):
        query = """
        select PO.owner_id, PO.company_symbol, PO.asset_type 
        from PURCHASE_ORDER as PO
        where PO.Asset_Type='{0}' AND PO.company_symbol='{1}'
        group by PO.owner_id, PO.company_symbol, PO.asset_type
        LIMIT {2};
        """.format(asset_type, company_symbol, limit)
        print(query)
        self.cur.execute(query)
        result = self.cur.fetchall()
        return result

    def list_owner_portfolio(self, owner_id):
        query = """
        select PO.company_symbol, PO.asset_type
        from PURCHASE_ORDER as PO
        where PO.Owner_ID={0}
        """.format(owner_id)
        self.cur.execute(query)
        result = self.cur.fetchall()
        return result

    def list_event_impact(self, date):
        query = """
        select GE.event_description, GE.time_stamp, GE.ap_news_link,
                dp.company_symbol,
                dp.asset_type, dp.close_dp, imp.industry_name
        from (
        select ev.industry_name
        from Event_M as ev
        where Date(ev.time_stamp)=Date('{0}')
        ) as imp, Company_Institution as c, Daily_Price as dp, Event_M as GE
        where c.industry_name in (imp.industry_name)
            AND DATE(dp.time_stamp) = Date('{1}')
            AND c.company_symbol=dp.company_symbol
            AND DATE(GE.time_stamp)=DATE(dp.time_stamp)
        """.format(date, date)
        self.cur.execute(query)
        result = self.cur.fetchall()
        return result

    def impact_by_location(self, date, limit):
        query = """
        select GE.event_description, GE.time_stamp, GE.ap_news_link,
            dp.company_symbol, dp.asset_type, dp.close_dp,
            imp.country_symbol
        from (
        select ev.country_symbol
        from Event_M as ev
        where Date(ev.time_stamp)=Date('{0}')
        ) as imp, Company_Institution as c, Daily_Price as dp, 
            Event_M as GE, Exchange_M as ex, Offers_Securities as offers
        where ex.country_symbol in (imp.country_symbol)
            AND DATE(dp.time_stamp)=DATE('{1}')
            AND DATE(GE.time_stamp)=DATE(dp.time_stamp)
            AND offers.company_symbol=dp.company_symbol
        group by GE.event_description, GE.time_stamp, GE.ap_news_link,
            dp.company_symbol, dp.asset_type, dp.close_dp,
            imp.country_symbol
        LIMIT {2}
        """.format(date, date, limit)
        self.cur.execute(query)
        result = self.cur.fetchall()
        return result

    def list_price_with_ir(self, date, price_type, country_symbol, limit):
        query = """
        select DP.company_symbol, DP.asset_type, DP.{0} AS price, DP.time_stamp, rf.current_rate
        from DAILY_PRICE as DP, Risk_Free_Rate as rf
        where Date(DP.time_stamp)=Date('{1}')
            AND Date(rf.time_stamp) = Date('{2}')
            AND rf.country_symbol='{3}'
        """.format(price_type, date, date, country_symbol, limit)
        print(query)
        self.cur.execute(query)
        result = self.cur.fetchall()
        return result

    def list_price_with_fin_data(self, comparator, date, company_symbol):
        query = """
        select DP.company_symbol, DP.asset_type, DP.high as price, fin.qtr_net_income
        from Financial_Data as fin, Daily_Price as DP
        where DATE(fin.time_stamp) {0} DATE('{1}')
	    AND DATE(fin.time_stamp) = (
		select DATE(max(f.time_stamp)) from financial_data as f where f.company_symbol = fin.company_symbol
        )
	    AND DATE(DP.time_stamp) = DATE('{2}')
        AND DP.company_symbol = '{3}'
        AND DP.company_symbol = fin.company_symbol
        """.format(comparator, date, date, company_symbol)
        self.cur.execute(query)
        result = self.cur.fetchall()
        return result

    def list_fin_data_event_impact(self, date, limit):
        query = """
        select fin.company_symbol, fin.qtr_net_income, ev.ap_news_link, ev.industry_name
        from Financial_Data as fin, Event_M as ev, Company_Institution as c
        where DATE(fin.time_stamp) <= DATE('{0}')
	    AND DATE(fin.time_stamp) = (
		    select DATE(max(f.time_stamp)) from financial_data as f where f.company_symbol = fin.company_symbol
        )
	    AND DATE(ev.time_stamp) <= DATE('{1}')
        AND ev.industry_name = c.industry_name
        AND c.company_symbol = fin.company_symbol
        Order By ev.time_stamp
        Limit {2}
        """.format(date, date, limit)
        print(query)
        self.cur.execute(query)
        result = self.cur.fetchall()
        return result
    
    def list_price_with_event_impact(self, date):
        query = """
        select dp.company_symbol, dp.high as price, ev.ap_news_link, ev.time_stamp
        from Event_M as ev, Company_Institution as c, Daily_Price as DP
        where DATE(dp.time_stamp) = DATE('{}')
	    AND DATE(ev.time_stamp) <= DATE(dp.time_stamp)
        AND c.industry_name = ev.industry_name
        AND dp.company_symbol = c.company_symbol
        order by ev.time_stamp desc
        """.format(date)
        self.cur.execute(query)
        result = self.cur.fetchall()
        return result
